﻿using System;

namespace OndeComer.ClassLibrary.models
{
    public class Voto
    {
        public long Id { get; private set; }
        public DateTime TimeStamp { get; private set; }

        public long UsuarioId { get; private set; }
        public Usuario Usuario { get; private set; }
        
        public long RestauranteId { get; private set; }
        public Restaurante Restaurante { get; private set; }

        public Voto()
        { }

        public Voto(Usuario usuario, Restaurante restaurante)
        {
            Usuario = usuario;
            Restaurante = restaurante;
            UsuarioId = usuario.Id;
            RestauranteId = restaurante.Id;
            TimeStamp = DateTime.Now;
        }
    }
}