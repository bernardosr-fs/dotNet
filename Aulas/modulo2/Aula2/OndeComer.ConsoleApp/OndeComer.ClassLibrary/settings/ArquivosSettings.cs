﻿namespace OndeComer.ClassLibrary.settings
{
    public class ArquivosSettings
    {
        public string UsuariosCsvPath { get; set; }
        public string RestaurantesCsvPath { get; set; }
        public string VotacoesCsvPath { get; set; }
        public string VotosCsvPath { get; set; }
    }
}
