﻿using OndeComer.ClassLibrary.models;
using System.Threading.Tasks;

namespace OndeComer.ClassLibrary.repositorios
{
    public interface IUsuarioRepositorio
    {
        Task<Usuario> ObterUsuario(long idUsuario);
        void InserirUsuario(Usuario usuario);
        void AtualizarUsuario(Usuario usuario);
    }
}
