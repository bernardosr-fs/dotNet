﻿
using OndeComer.ClassLibrary.models;
using System.Threading.Tasks;

namespace OndeComer.ClassLibrary.repositorios
{
    public interface IVotacaoRepositorio
    {
        Task<Votacao> ObterSessaoVotacao();
        Task<Voto> ObterVotoPorUsuarioId(long usuarioId);
        void InserirVoto(Voto voto);
        void AlterarVotacao(Votacao votacao);
    }
}
