﻿namespace OndeComer.WebApi.Requests
{
    public class RealizaVotacaoRequest
    {
        public int IdUsuario { get; set; }
        public int IdRestaurante { get; set; }
    }
}
