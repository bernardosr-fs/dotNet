﻿using OndeComer.ClassLibrary.models;
using OndeComer.WebApi.Requests;

namespace OndeComer.WebApi.Mappers
{
    public static class UsuarioRequestMapper
    {
        public static Usuario Map(this InsereUsuarioRequest insereUsuarioRequest)
        {
            return new Usuario(insereUsuarioRequest.Id,
                                insereUsuarioRequest.Nome);
        }
    }
}
