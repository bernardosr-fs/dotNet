﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OndeComer.ClassLibrary.models;

namespace OndeComer.Infra.Mappings
{
    public class VotoMapping : IEntityTypeConfiguration<Voto>
    {
        public void Configure(EntityTypeBuilder<Voto> builder)
        {
            builder.Property(c => c.Id)
                .ValueGeneratedOnAdd();

            builder.Property(c => c.TimeStamp);

            builder.Property(c => c.UsuarioId);

            builder.Property(c => c.RestauranteId);

            builder.HasOne(c => c.Usuario);

            builder.HasOne(c => c.Restaurante);
        }
    }
}
 