﻿using Microsoft.EntityFrameworkCore;
using OndeComer.ClassLibrary.models;
using OndeComer.ClassLibrary.repositorios;
using OndeComer.Infra.Context;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace OndeComer.Infra.RepositoriosPostgres
{
    public class VotacaoRepositorioPostgres : IVotacaoRepositorio
    {
        private readonly OndeComerDbContext _ondeComerDbContext;
        private readonly DateTime dataAtual = DateTime.Now;

        public VotacaoRepositorioPostgres(OndeComerDbContext ondeComerDbContext)
        {
            _ondeComerDbContext = ondeComerDbContext;
        }

        public async Task<Votacao> ObterSessaoVotacao()
        {
            var votosDoDia = _ondeComerDbContext.Votos
                .Include(x => x.Restaurante)
                .Include(x => x.Usuario)
                .Where(x => DentroDaDataEspecificada(x)).ToList();

            return new Votacao(dataAtual, votosDoDia);
        }

        public void InserirVoto(Voto voto)
        {
            _ondeComerDbContext.Votos.Add(voto);
            _ondeComerDbContext.SaveChanges();
        }

        public void AlterarVotacao(Votacao votacao)
        {
            throw new System.NotImplementedException();
        }

        public async Task<Voto> ObterVotoPorUsuarioId(long usuarioId)
        {
            return await _ondeComerDbContext.Votos.FirstOrDefaultAsync(v => v.UsuarioId == usuarioId && DentroDaDataEspecificada(v));
        }

        private bool DentroDaDataEspecificada(Voto voto)
            => voto.TimeStamp > dataAtual.Date && voto.TimeStamp < dataAtual.Date.AddDays(1);
    }
}
