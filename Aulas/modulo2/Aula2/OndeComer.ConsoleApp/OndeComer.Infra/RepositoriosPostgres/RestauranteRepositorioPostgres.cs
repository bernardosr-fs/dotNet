﻿using Microsoft.EntityFrameworkCore;
using OndeComer.ClassLibrary.models;
using OndeComer.ClassLibrary.repositorios;
using OndeComer.Infra.Context;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OndeComer.Infra.RepositoriosPostgres
{
    public class RestauranteRepositorioPostgres : IRestauranteRepositorio
    {
        private readonly OndeComerDbContext _ondeComerDbContext;

        public RestauranteRepositorioPostgres(OndeComerDbContext ondeComerDbContext)
        {
            _ondeComerDbContext = ondeComerDbContext;
        }

        public void InserirRestaurante(Restaurante restaurante)
        {
            _ondeComerDbContext.Restaurantes.Add(restaurante);
            _ondeComerDbContext.SaveChanges();
        }

        public async Task<Restaurante> ObterInteralRestaurante(long idRestaurante)
        {
            return await _ondeComerDbContext.Restaurantes.FirstOrDefaultAsync(x => x.Id == idRestaurante);
        }

        public async Task<Restaurante> ObterRestaurante(long idRestaurante)
        {
            return await _ondeComerDbContext.Restaurantes.FirstOrDefaultAsync(x => x.Id == idRestaurante);
        }

        public async Task<List<Restaurante>> ListarRestaurante()
        {
            return await _ondeComerDbContext.Restaurantes.AsNoTracking().ToListAsync();
        }

        public void AtualizarRestaurante(Restaurante restaurante)
        {
            _ondeComerDbContext.Restaurantes.Update(restaurante);
            _ondeComerDbContext.SaveChanges();
        }
    }
}
