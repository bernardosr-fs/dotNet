﻿namespace OndeComer.ClassLibrary.Dtos
{
    public class RestauranteDto
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public int QuantidadeLugares { get; set; }
        public EnderecoDto Endereco { get; set; }
    }
}
