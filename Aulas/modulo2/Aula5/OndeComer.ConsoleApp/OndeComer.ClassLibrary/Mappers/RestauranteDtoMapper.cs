﻿using OndeComer.ClassLibrary.Dtos;
using OndeComer.ClassLibrary.models;

namespace OndeComer.ClassLibrary.Mappers
{
    public static class RestauranteDtoMapper
    {
        public static Restaurante Map(this RestauranteDto restauranteDto)
        {
            return new Restaurante(restauranteDto.Id, restauranteDto.Nome);
        }
    }
}
