﻿using OndeComer.ClassLibrary.Dtos;
using OndeComer.ClassLibrary.Dtos.Validators;
using OndeComer.ClassLibrary.Mappers;
using OndeComer.ClassLibrary.models;
using OndeComer.ClassLibrary.repositorios;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OndeComer.ClassLibrary.services
{
    public class RestauranteService
    {
        private readonly IRestauranteRepositorio _restauranteRepositorio;
        private readonly RestauranteDtoValidator _restauranteDtoValidator;

        public RestauranteService(RestauranteDtoValidator restauranteDtoValidator, IRestauranteRepositorio restauranteRepositorio)
        {
            _restauranteRepositorio = restauranteRepositorio;
            _restauranteDtoValidator = restauranteDtoValidator;
        }

        public async Task<bool> InserirRestaurante(RestauranteDto restaurante)
        {
            //var validator = new RestauranteDtoValidator();
            var validationResult = _restauranteDtoValidator.Validate(restaurante);

            if (!validationResult.IsValid)
            {
                var teste = validationResult.Errors;
                throw new Exception(validationResult.Errors.ToString());
            }

            var restauranteModel = restaurante.Map();

            _restauranteRepositorio.InserirRestaurante(restauranteModel);

            return true;
        }

        public async Task<Restaurante> ObterRestaurante(int idRestaurante)
        {
            return await _restauranteRepositorio.ObterRestaurante(idRestaurante);
        }

        public async Task<List<Restaurante>> ListarRestaurante()
        {
            return await _restauranteRepositorio.ListarRestaurante();
        }

        public async Task AtualizarRestaurante(RestauranteDto restauranteDto)
        {

            var restauranteASerAtualizado = await _restauranteRepositorio.ObterInteralRestaurante(restauranteDto.Id);

            if (restauranteASerAtualizado == null)
                return;

            var restauranteModel = restauranteDto.Map();

            restauranteASerAtualizado.Atualizar(restauranteModel);

            _restauranteRepositorio.AtualizarRestaurante(restauranteModel);
        }
    }
}
