﻿using OndeComer.ClassLibrary.models;
using OndeComer.ClassLibrary.repositorios;
using System.Threading.Tasks;

namespace OndeComer.ClassLibrary.services
{
    public class UsuarioService
    {
        private readonly IUsuarioRepositorio _usuarioRepositorio;

        public UsuarioService(IUsuarioRepositorio usuarioRepositorio)
        {
            _usuarioRepositorio = usuarioRepositorio;
        }

        public async Task<bool> InserirUsuario(Usuario usuario)
        {
            _usuarioRepositorio.InserirUsuario(usuario);

            return true;
        }

        public async Task<Usuario> ObterUsuario(int idUsuario)
        {
            return await _usuarioRepositorio.ObterUsuario(idUsuario);
        }

        public async Task AtualizarUsuario(Usuario usuario)
        {
            var usuarioASerAtualizado = await _usuarioRepositorio.ObterUsuario(usuario.Id);

            if (usuarioASerAtualizado == null)
                return;

            usuarioASerAtualizado.Atualizar(usuario);

            _usuarioRepositorio.AtualizarUsuario(usuarioASerAtualizado);
        }
    }
}
