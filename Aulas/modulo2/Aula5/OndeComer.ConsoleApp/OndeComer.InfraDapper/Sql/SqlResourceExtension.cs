﻿using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace OndeComer.InfraDapper.Sql
{
    public class SqlResourceExtension
    {
        private static readonly string scritps = "Scripts";
        public static readonly string ListarRestaurantes = GetSqlResource(scritps, "ListarRestaurantes");
        public static readonly string InserirResutaurante = GetSqlResource(scritps, "InserirRestaurante");
        public static readonly string ListarVotos = GetSqlResource(scritps, "ListarVotos");

        private static string GetSqlResource(string directory, string file)
        {
            var assemblyName = $"OndeComer.InfraDapper.Sql.{directory}.{file}.sql";

            try
            {
                var assembly = Assembly.GetExecutingAssembly();
                var resourceStream = assembly.GetManifestResourceStream(assemblyName);

                using (var reader = new StreamReader(resourceStream, Encoding.UTF8))
                {
                    return reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Não foi possível carregar o Assembly: {assemblyName}", ex);
            }
        }
    }
}
