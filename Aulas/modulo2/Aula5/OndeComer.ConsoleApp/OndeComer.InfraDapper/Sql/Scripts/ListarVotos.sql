﻿SELECT 
	v."Id" AS Id, 
    v."TimeStamp",
    v."UsuarioId",
    v."RestauranteId",
    v."UsuarioId" AS Id,
    u."Nome",
    v."RestauranteId" AS Id,
    r."Nome"
FROM
    "Votos" v
inner join "Usuarios" u on u."Id" = v."UsuarioId"
inner join "Restaurantes" r on r."Id" = v."RestauranteId"