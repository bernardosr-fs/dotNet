﻿using Dapper;
using Npgsql;
using OndeComer.ClassLibrary.models;
using OndeComer.ClassLibrary.repositorios;
using OndeComer.InfraDapper.Configuration;
using OndeComer.InfraDapper.Sql;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace OndeComer.InfraDapper.Repositories
{
    public class VotacaoDapperRepository : IVotacaoRepositorio
    {
        private readonly PostgresConfiguration _postgresConfiguration;
        public VotacaoDapperRepository(PostgresConfiguration postgresConfiguration)
        {
            _postgresConfiguration = postgresConfiguration;
        }

        public void AlterarVotacao(Votacao votacao)
        {
            throw new System.NotImplementedException();
        }

        public void InserirVoto(Voto voto)
        {
            throw new System.NotImplementedException();
        }

        public Votacao ObterSessaoVotacao()
        {
            throw new NotImplementedException();
        }

        public async Task<Votacao> ObterSessaoVotacaoDapper()
        {
            using (var db = new NpgsqlConnection(_postgresConfiguration.ConnectionString))
            {
                var result = await db.QueryAsync<Voto, Usuario, Restaurante, Voto>(SqlResourceExtension.ListarVotos,
                    (votacao, usuario, restaurante) =>
                    {
                        votacao.AtribuirUsuario(usuario);
                        votacao.AtribuirRestaurante(restaurante);

                        return votacao;
                    }
                    ,
                    new
                    {
                        DataHoje = DateTime.Now.Date,
                        DataAmanha = DateTime.Now.Date.AddDays(1)
                    });

                return new Votacao(DateTime.Now, result.ToList());
            }
        }

        public Task<Voto> ObterVotoPorUsuarioId(long usuarioId)
        {
            throw new System.NotImplementedException();
        }
    }
}
