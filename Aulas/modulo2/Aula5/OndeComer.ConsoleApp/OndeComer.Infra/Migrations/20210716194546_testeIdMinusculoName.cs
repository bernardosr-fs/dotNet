﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OndeComer.Infra.Migrations
{
    public partial class testeIdMinusculoName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Id",
                table: "voto",
                newName: "id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "id",
                table: "voto",
                newName: "Id");
        }
    }
}
