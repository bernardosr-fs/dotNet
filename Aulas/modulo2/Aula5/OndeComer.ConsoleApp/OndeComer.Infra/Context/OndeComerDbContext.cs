﻿using Microsoft.EntityFrameworkCore;
using OndeComer.ClassLibrary.models;
using OndeComer.Infra.Mappings;

namespace OndeComer.Infra.Context
{
    public class OndeComerDbContext : DbContext
    {
        public OndeComerDbContext(DbContextOptions<OndeComerDbContext> dbContextOptions) : base (dbContextOptions)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new RestauranteMapping());
            modelBuilder.ApplyConfiguration(new UsuarioMapping());
            modelBuilder.ApplyConfiguration(new VotoMapping());
        }

        public DbSet<Restaurante> Restaurantes { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Voto> Votos { get; set; }
    }
}
