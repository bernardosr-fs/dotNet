﻿using Microsoft.EntityFrameworkCore;
using OndeComer.ClassLibrary.models;
using OndeComer.ClassLibrary.repositorios;
using OndeComer.Infra.Context;
using System.Threading.Tasks;

namespace OndeComer.Infra.RepositoriosPostgres
{
    public class UsuarioRepositorioPostgres : IUsuarioRepositorio
    {
        private readonly OndeComerDbContext _ondeComerDbContext;

        public UsuarioRepositorioPostgres(OndeComerDbContext ondeComerDbContext)
        {
            _ondeComerDbContext = ondeComerDbContext;
        }

        public void AtualizarUsuario(Usuario usuario)
        {
            throw new System.NotImplementedException();
        }

        public void InserirUsuario(Usuario usuario)
        {
            _ondeComerDbContext.Usuarios.Add(usuario);
            _ondeComerDbContext.SaveChanges();
        }

        public async Task<Usuario> ObterUsuario(long idUsuario)
        {
            return await _ondeComerDbContext.Usuarios.FirstOrDefaultAsync(u => u.Id == idUsuario);
        }
    }
}
