﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OndeComer.ConsoleApp.models
{
    public class Votacao
    {
        public DateTime Data { get; }
        public List<Voto> Votos { get; } = new List<Voto>();

        public Votacao(DateTime data)
        {
            Data = data;
        }

        public void AdicionarVoto(Voto voto)
        {
            Votos.Add(voto);
        }



        public void ImprimirVotos()
        {
            //var restaurantesVotados2 = from voto in Votos
            //                           select voto.Restaurante;
            var restaurantesVotados = Votos.Select(x => x.Restaurante)
                                            .Distinct(new Restaurante.RestauranteComparer());

            Console.WriteLine($"Inicio Resultado");

            foreach (var restauranteVotado in restaurantesVotados)
                Console.WriteLine($"Restaurante: {restauranteVotado.Nome}\nQtd Votos: {Votos.Count(x => x.Restaurante.Id == restauranteVotado.Id)}");

            Console.WriteLine($"Fim Resultado");
        }
    }
}
