﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace OndeComer.ConsoleApp.models
{
    public class Restaurante
    {
        public int Id { get; }
        public string Nome { get; }

        public Restaurante(int id, string nome)
        {
            Id = id;
            Nome = nome;
        }

        public class RestauranteComparer : IEqualityComparer<Restaurante>
        {
            public bool Equals(Restaurante x, Restaurante y)
            {
                return x.Id == y.Id;
            }

            public int GetHashCode([DisallowNull] Restaurante obj)
            {
                if (obj == null)
                    return 0;

                return obj.Id;
            }
        }
    }
}
