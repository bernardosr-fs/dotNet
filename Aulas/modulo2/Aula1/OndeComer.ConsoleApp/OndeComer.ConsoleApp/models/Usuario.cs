﻿namespace OndeComer.ConsoleApp.models
{
    public class Usuario
    {
        public int Id { get; }
        public string Nome { get; }

        public Usuario(int id, string nome)
        {
            Id = id;
            Nome = nome;
        }
    }
}
