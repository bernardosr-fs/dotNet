﻿namespace OndeComer.ConsoleApp.models
{
    public class Voto
    {
        public Usuario Usuario { get; }
        public Restaurante Restaurante { get; }

        public Voto(Usuario usuario, Restaurante restaurante)
        {
            Usuario = usuario;
            Restaurante = restaurante;
        }
    }
}