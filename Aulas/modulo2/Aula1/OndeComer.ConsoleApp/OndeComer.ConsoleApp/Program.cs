﻿using OndeComer.ConsoleApp.repositorios;
using OndeComer.ConsoleApp.services;
using System;

namespace OndeComer.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var usuarioRepositorioCsv = new UsuarioRepositorioCsv();
            var restauranteRepositorioCsv = new RestauranteRepositorioCsv();
            var votacaoService = new VotacaoService(usuarioRepositorioCsv, restauranteRepositorioCsv);

            votacaoService.RealizarVotacao(1, 1);
            votacaoService.RealizarVotacao(2, 2);
            votacaoService.RealizarVotacao(3, 1);
        }
    }
}
