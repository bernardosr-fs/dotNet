﻿using OndeComer.ConsoleApp.models;

namespace OndeComer.ConsoleApp.repositorios
{
    public interface IUsuarioRepositorio
    {
        Usuario ObterUsuario(int idUsuario);
    }
}
