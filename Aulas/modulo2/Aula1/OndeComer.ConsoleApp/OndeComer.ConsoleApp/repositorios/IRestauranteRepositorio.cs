﻿using OndeComer.ConsoleApp.models;

namespace OndeComer.ConsoleApp.repositorios
{
    public interface IRestauranteRepositorio
    {
        Restaurante ObterRestaurante(int idRestaurante);
    }
}
