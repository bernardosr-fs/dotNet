﻿using OndeComer.ConsoleApp.models;
using System.IO;

namespace OndeComer.ConsoleApp.repositorios
{
    public class RestauranteRepositorioCsv : IRestauranteRepositorio
    {
        public Restaurante ObterRestaurante(int idRestaurante)
        {
            var linhas = File.ReadAllLines(@".\restaurantes.csv");

            foreach (var linha in linhas)
            {
                var colunas = linha.Split(',');
                var colunaId = int.Parse(colunas[0]);
                var colunaNome = colunas[1];

                if (colunaId == idRestaurante)
                    return new Restaurante(colunaId, colunaNome);
            }

            return null;
        }
    }
}
