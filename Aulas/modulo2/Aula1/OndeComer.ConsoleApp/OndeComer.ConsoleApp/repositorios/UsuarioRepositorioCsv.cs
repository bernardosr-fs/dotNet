﻿using OndeComer.ConsoleApp.models;
using System.IO;

namespace OndeComer.ConsoleApp.repositorios
{
    public class UsuarioRepositorioCsv : IUsuarioRepositorio
    {
        public Usuario ObterUsuario(int idUsuario)
        {
            var linhas = File.ReadAllLines(@".\usuarios.csv");

            foreach (var linha in linhas)
            {
                var colunas = linha.Split(',');
                var colunaId = int.Parse(colunas[0]);
                var colunaNome = colunas[1];

                if (colunaId == idUsuario)
                    return new Usuario(colunaId, colunaNome);
            }

            return null;
        }
    }
}
