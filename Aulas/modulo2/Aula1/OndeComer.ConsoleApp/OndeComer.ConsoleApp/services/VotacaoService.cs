﻿using OndeComer.ConsoleApp.models;
using OndeComer.ConsoleApp.repositorios;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OndeComer.ConsoleApp.services
{
    public class VotacaoService
    {
        private static readonly List<Votacao> _votacoes = new List<Votacao>();

        private readonly IUsuarioRepositorio _usuarioRepositorio;
        private readonly IRestauranteRepositorio _restauranteRepositorio;

        public VotacaoService(IUsuarioRepositorio usuarioRepositorio, IRestauranteRepositorio restauranteRepositorio)
        {
            _usuarioRepositorio = usuarioRepositorio;
            _restauranteRepositorio = restauranteRepositorio;
        }

        public void RealizarVotacao(int idUsuario, int idRestaurante)
        {
            var usuario = _usuarioRepositorio.ObterUsuario(idUsuario);

            if (usuario == null)
                throw new Exception("Usuário não existe");

            var restaurante = _restauranteRepositorio.ObterRestaurante(idRestaurante);

            var sessaoVotacao = ObterSessaoVotacao();

            var voto = new Voto(usuario, restaurante);
            sessaoVotacao.AdicionarVoto(voto);

            sessaoVotacao.ImprimirVotos();
        }

        private Votacao ObterSessaoVotacao()
        {
            var votacao = _votacoes.FirstOrDefault(x => x.Data.Date == DateTime.Now.Date);

            if (votacao == null)
            {
                votacao = new Votacao(DateTime.Now);
                _votacoes.Add(votacao);
            }

            return votacao;
        }
    }
}
