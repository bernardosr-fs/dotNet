using OndeComer.ConsoleApp.repositorios;
using OndeComer.ConsoleApp.services;
using System;
using Xunit;

namespace OndeComer.ConsoleApp.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void DadoUsuarioParaVotar_QuandoNaoExisteNaBase_EntaoDeveRetornarExcecao()
        {
            //Arrange
            var usuarioRepositorio = new UsuarioRepositorioCsv();
            var restauranteRepositorio = new RestauranteRepositorioCsv();
            var votacaoService = new VotacaoService(usuarioRepositorio, restauranteRepositorio);

            //Act
            var result = Assert.Throws<Exception>(() => votacaoService.RealizarVotacao(120, 1));

            //Assert
            Assert.Equal("Usu�rio n�o existe", result.Message);
        }


        [Theory]
        [InlineData(121,333)]
        [InlineData(122, 333)]
        [InlineData(123, 333)]
        [InlineData(124, 333)]
        [InlineData(125, 333)]
        public void DadoUsuariosParaVotar_QuandoNaoExistemNaBase_EntaoDeveRetornarExcecao(int idUsuario, int idRestaurante)
        {
            //Arrange
            var usuarioRepositorio = new UsuarioRepositorioCsv();
            var restauranteRepositorio = new RestauranteRepositorioCsv();
            var votacaoService = new VotacaoService(usuarioRepositorio, restauranteRepositorio);

            //Act
            var result = Assert.Throws<Exception>(() => votacaoService.RealizarVotacao(idUsuario, idRestaurante));

            //Assert
            Assert.Equal("Usu�rio n�o existe", result.Message);
        }
    }
}
