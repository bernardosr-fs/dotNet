﻿namespace Crescer.Aula3.Ppt
{
    internal class Pessoa
    {
        public string Nome { get; set; }
        public int Idade { get; set; }

        public override string ToString()
        {
            return Nome;
        }
    }
}