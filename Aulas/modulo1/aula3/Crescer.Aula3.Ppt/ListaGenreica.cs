﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Crescer.Aula3.Ppt
{
    internal class ListaGenreica<T> //where T : Pessoa
    {
        private No _ultimo;

        class No
        {
            public T Dado { get; set; }
            public No Anterior { get; set; }
        }

        internal void Inserir(T pessoa)
        {
            var no = new No();
            no.Dado = pessoa;
            no.Anterior = _ultimo;

            _ultimo = no;
        }

        public IEnumerator<T> GetEnumerator()
        {
            var aux = _ultimo;

            while (aux != null)
            {
                yield return aux.Dado;
                aux = aux.Anterior;
            }
        }
    }
}