﻿using System;
using System.Collections;

namespace Crescer.Aula3.Ppt
{
    internal class Lista
    {
        private No _ultimo;

        class No
        {
            public object Dado { get; set; }
            public No Anterior { get; set; }
        }

        internal void Inserir(object pessoa)
        {
            var no = new No();
            no.Dado = pessoa;
            no.Anterior = _ultimo;

            _ultimo = no;
        }

        public IEnumerator GetEnumerator()
        {
            var aux = _ultimo;

            while (aux != null)
            {
                yield return aux.Dado;
                aux = aux.Anterior;
            }
        }
    }
}