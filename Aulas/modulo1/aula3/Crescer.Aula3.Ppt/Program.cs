﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Crescer.Aula3.Ppt
{
    class Program
    {
        static async Task Main(string[] args)
        {
            /*
            var lista = new ListaGenerica<Pessoa>();

            for (int i = 0; i < 5; i++)
            {
                lista.Inserir(new Pessoa { Nome = "Pessoa" + i });
            }

            foreach (var pessoa in lista)
            {
                Pessoa teste = pessoa;
                Console.WriteLine(teste.Nome);
            }
            */

            /*
            var lista = new List<Pessoa>();

            for (int i = 0; i < 5; i++)
            {
                lista.Add(new Pessoa { Nome = "Pessoa" + i, Idade = i });
            }

            var selecao =
                    from p in lista
                    where p.Idade > 2
                    select p.Nome;

            var selecao2 = lista.Where((x) => x.Idade > 2).Select(x => x.Nome);
            */

            /*
            var thread = new Thread(() =>
            {
                for (int i = 0; i < 5; i++)
                {
                    Console.WriteLine("Hello World! from anothe thread" + i);
                    Thread.Sleep(1000);
                }
            });

            thread.Start();

            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Hello World!" + i);
                Thread.Sleep(200);
            }

            thread.Join(); // espera
            Console.WriteLine("Fim");
            */
            ConcorrentLi

            await Task.Run(() =>
            {
                for (int i = 0; i < 5; i++)
                {
                    Console.WriteLine("Hello World! from anothe thread" + i);
                    Thread.Sleep(1000);
                }
            });

            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Hello World!" + i);
                Thread.Sleep(200);
            }
            //Task.WaitAll
            Console.WriteLine("Fim");
        }

        static async Task Teste() // errado
        {
            /*
            lock (obj)
            {

            }
            */

            await Task.Run(() =>
            {
                for (int i = 0; i < 5; i++)
                {
                    Console.WriteLine("Hello World! from anothe thread" + i);
                    Thread.Sleep(1000);
                }
            });
        }

        static Task Teste2() //mello
        {
            
            return Task.Run(() =>
            {
                for (int i = 0; i < 5; i++)
                {
                    Console.WriteLine("Hello World! from anothe thread" + i);
                    Thread.Sleep(1000);
                }
            });
        }

        static Task<int> comValor()
        {
            return Task.FromResult(1);
        }
    }
}
