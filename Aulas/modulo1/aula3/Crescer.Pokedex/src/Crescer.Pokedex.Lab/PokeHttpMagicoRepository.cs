﻿using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace Crescer.Pokedex.Lab
{
    public class PokeHttpMagicoRepository : IPokemonRepository
    {
        private HttpClient _httpClient = new();

        public PokeHttpMagicoRepository()
        {
            _httpClient.BaseAddress = new Uri("https://pokeapi.co/api/v2/");
        }

        public async Task<Pokemon> BuscarPokemon(string nome)
        {
            var response = await _httpClient.GetStreamAsync($"pokemon/{nome}");
            var pokemon = await JsonSerializer.DeserializeAsync<PokemonModel>(response);
            return new Pokemon
            {
                Nome = pokemon.name,
                Altura = pokemon.height,
                Peso = pokemon.weight
            };
        }

        public void Dispose()
        {
            _httpClient.Dispose();
        }
    }
}
