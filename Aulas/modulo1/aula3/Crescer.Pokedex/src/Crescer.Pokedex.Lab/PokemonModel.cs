﻿namespace Crescer.Pokedex.Lab
{
    internal class PokemonModel
    {
        public string name { get; set; }
        public int weight { get; set; }
        public int height { get; set; }
    }
}