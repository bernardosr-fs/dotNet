﻿using PokeApiNet;
using System;
using System.Threading.Tasks;

namespace Crescer.Pokedex.Lib
{
    public class PokeApiLibRepository : IPokemonRepository
    {
        private readonly PokeApiClient _client = new();

        public async Task<Pokemon> BuscarPokemon(string nome)
        {
            var pokemon = await _client.GetResourceAsync<PokeApiNet.Pokemon>(nome); //.GetAwaiter().GetResult();
            return new Pokemon
            {
                Nome = pokemon.Name,
                Altura = pokemon.Height,
                Peso = pokemon.Weight
            };
        }

        public void Dispose()
        {
            _client.Dispose();
        }
    }
}
