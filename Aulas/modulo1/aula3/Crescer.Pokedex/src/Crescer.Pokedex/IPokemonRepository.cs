﻿using System;
using System.Threading.Tasks;

namespace Crescer.Pokedex
{
    public interface IPokemonRepository : IDisposable
    {
        Task<Pokemon> BuscarPokemon(string nome);
    }
}