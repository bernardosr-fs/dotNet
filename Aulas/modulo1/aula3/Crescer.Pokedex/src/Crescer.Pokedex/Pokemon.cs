﻿using System;

namespace Crescer.Pokedex
{
    public class Pokemon
    {
        public string Nome { get; set; }
        public int Altura { get; set; }
        public int Peso { get; set; }
    }
}
