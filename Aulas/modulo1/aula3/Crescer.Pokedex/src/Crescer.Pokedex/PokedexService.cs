﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crescer.Pokedex
{
    public class PokedexService : IDisposable
    {
        private readonly IPokemonRepository _pokemonRepository;
        private readonly List<Pokemon> _pokemons = new();

        public PokedexService(IPokemonRepository pokemonRepository)
        {
            _pokemonRepository = pokemonRepository;
        }

        public async Task AdicionarPokemon(string nome)
        {
            var pokemon = await _pokemonRepository.BuscarPokemon(nome);
            _pokemons.Add(pokemon);
        }

        public void Dispose()
        {
            _pokemonRepository.Dispose();
        }

        public IEnumerable<Pokemon> ListarPokemons() => _pokemons;
    }
}
