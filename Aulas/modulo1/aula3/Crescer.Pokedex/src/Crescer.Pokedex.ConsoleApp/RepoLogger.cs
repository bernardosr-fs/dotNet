﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crescer.Pokedex.ConsoleApp
{
    class RepoLogger : IPokemonRepository
    {
        private readonly IPokemonRepository _repository;

        public RepoLogger(IPokemonRepository repository)
        {
            this._repository = repository;
        }

        public async Task<Pokemon> BuscarPokemon(string nome)
        {
            var timer = Stopwatch.StartNew();
            var res = await _repository.BuscarPokemon(nome);
            timer.Stop();
            Console.WriteLine($"Buscou por: {nome} em {timer.ElapsedMilliseconds}ms");
            return res;
        }

        public void Dispose()
        {
            _repository.Dispose();
        }
    }
}
