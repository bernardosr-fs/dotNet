﻿using Crescer.Pokedex.Lab;
using Crescer.Pokedex.Lib;
using System;
using System.Threading.Tasks;

namespace Crescer.Pokedex.ConsoleApp
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var repo1 = new PokeApiLibRepository();
            var repo2 = new PokeHttpMagicoRepository();
            var repoLog = new RepoLogger(repo1);

            using (var pokedex = new PokedexService(repoLog))
            {
                await pokedex.AdicionarPokemon("ditto");

                Console.WriteLine("+------------------------------------------------------------------+");
                Console.WriteLine("| Nome                          | Altura        | Peso             |");
                Console.WriteLine("+------------------------------------------------------------------+");
                foreach (Pokemon pokemon in pokedex.ListarPokemons())
                {
                    Console.WriteLine($"| {pokemon.Nome,-30}| {pokemon.Altura,-14}| {pokemon.Peso.ToString().PadRight(17, ' ')}|");
                }
                Console.WriteLine("+------------------------------------------------------------------+");
            }
        }
    }
}
