using System;

namespace Crescer.Aula2.LibMagica
{
    public class Cachorro : Animal, IServivoVivo
    {
        public string Raca { get; }

        public Cachorro(string nome, string raca, DateTime? dataNascimento = null)
            : base(nome, dataNascimento)
        {
            Raca = raca;
        }
        public override string DigaAlgo()
        {
            throw new NotImplementedException();
        }
    }
}