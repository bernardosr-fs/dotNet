﻿using System;

namespace Crescer.Aula2.LibMagica
{
    public abstract class Animal
    {
        private Options options = new Options();
        protected int coracao;

        public string Nome { get; }
        public DateTime DataNascimento { get; }

        public Animal(string nome, DateTime? dataNascimento = null)
        {
            if (!dataNascimento.HasValue)
                dataNascimento = DateTime.Now;

            Nome = nome;
            DataNascimento = dataNascimento.Value;
        }

        public abstract string DigaAlgo();

        public void Travessura(Action acao)
        {
            Console.WriteLine("@#$%ˆ&*ˆ%$#");
            acao();
        }

        public void Configurar(Action<Options> acao)
        {
            acao(options);
        }

        public void IsFaleu(Func<bool> acao)
        {
            if (acao())
            {
                ///
            }
            Console.WriteLine("@#$%ˆ&*ˆ%$#");
            acao();
        }

        public void Som()
        {
            throw new NotImplementedException();
        }
    }
}
