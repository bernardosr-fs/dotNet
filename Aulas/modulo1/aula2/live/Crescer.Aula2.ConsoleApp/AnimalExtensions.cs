using System;
using Crescer.Aula2.LibMagica;

namespace Crescer.Aula2.ConsoleApp.Extensions
{
    public static class AnimalExtensions
    {
        public static void EscreverNome(this Animal source)
            => Console.WriteLine(source.Nome);
    }
}