﻿using System;
using Crescer.Aula2.LibMagica;
using Crescer.Aula2.ConsoleApp.Extensions;

namespace Crescer.Aula2.ConsoleApp
{
    class Program
    {
        static void acao()
        {

        }

        static void Main(string[] args)
        {
            int? b = 1;
            char[] a = null;
            var cachorro = new Cachorro("bilu", "rasga");
            cachorro.EscreverNome();

            cachorro.Configurar((o) => {
                var patas = int.Parse(Console.ReadLine());
                o.Patas = patas;
            });
            cachorro.Travessura(acao);
            cachorro.Travessura(() => {
                Console.Beep();
            });

            cachorro.IsFaleu(() => {
                return true;
            });
            Console.WriteLine(cachorro.Raca);

            Tipo tipo = Tipo.Terrestre;
            Console.WriteLine((int)tipo);
            //Func
            //Action

            /*
            FileStream arquivo2 = File.Open(@"", FileMode.Open);
            try
            {
                using (FileStream arquivo = File.Open(@"", FileMode.Open))
                {

                }
            }
            finally
            {
                arquivo2.Dispose();
            }
            */
        }
    }
}
