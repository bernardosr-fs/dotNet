﻿using Newtonsoft.Json;

namespace OndeComer.Infra.ExternalService.Dtos
{
    public class LoginRequestDto
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
