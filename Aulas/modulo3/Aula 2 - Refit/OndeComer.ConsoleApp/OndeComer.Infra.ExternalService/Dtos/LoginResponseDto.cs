﻿namespace OndeComer.Infra.ExternalService.Dtos
{
    public class LoginResponseDto
    {
        public string Token { get; set; }
    }
}
