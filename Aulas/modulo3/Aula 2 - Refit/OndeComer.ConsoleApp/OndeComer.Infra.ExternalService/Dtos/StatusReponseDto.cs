﻿namespace OndeComer.Infra.ExternalService.Dtos
{
    public class StatusReponseDto
    {
        public bool Api { get; set; }

        public bool Db { get; set; }
    }
}
