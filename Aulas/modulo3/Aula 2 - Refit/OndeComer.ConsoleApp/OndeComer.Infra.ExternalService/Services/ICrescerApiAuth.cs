﻿using OndeComer.Infra.ExternalService.Dtos;
using Refit;
using System.Threading.Tasks;

namespace OndeComer.Infra.ExternalService.Services
{
    public interface ICrescerApiAuth
    {
        [Post("/login")]
        public Task<LoginResponseDto> Login(LoginRequestDto request);

        [Get("/me")]
        public Task<UsuarioResponseDto> ObterUsuario();

        [Post("/register")]
        public Task<RegistrarResponseDto> Registrar(RegistrarRequestDto request);

        [Get("/stat")]
        public Task<StatusReponseDto> Status();

        [Post("/logout")]
        public Task Logout();

        [Delete("/me")]
        public Task Deletar();
    }
}
