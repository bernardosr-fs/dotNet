﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OndeComer.Infra.Migrations
{
    public partial class testeTableName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Votos_Restaurantes_RestauranteId",
                table: "Votos");

            migrationBuilder.DropForeignKey(
                name: "FK_Votos_Usuarios_UsuarioId",
                table: "Votos");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Votos",
                table: "Votos");

            migrationBuilder.RenameTable(
                name: "Votos",
                newName: "voto");

            migrationBuilder.RenameIndex(
                name: "IX_Votos_UsuarioId",
                table: "voto",
                newName: "IX_voto_UsuarioId");

            migrationBuilder.RenameIndex(
                name: "IX_Votos_RestauranteId",
                table: "voto",
                newName: "IX_voto_RestauranteId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_voto",
                table: "voto",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_voto_Restaurantes_RestauranteId",
                table: "voto",
                column: "RestauranteId",
                principalTable: "Restaurantes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_voto_Usuarios_UsuarioId",
                table: "voto",
                column: "UsuarioId",
                principalTable: "Usuarios",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_voto_Restaurantes_RestauranteId",
                table: "voto");

            migrationBuilder.DropForeignKey(
                name: "FK_voto_Usuarios_UsuarioId",
                table: "voto");

            migrationBuilder.DropPrimaryKey(
                name: "PK_voto",
                table: "voto");

            migrationBuilder.RenameTable(
                name: "voto",
                newName: "Votos");

            migrationBuilder.RenameIndex(
                name: "IX_voto_UsuarioId",
                table: "Votos",
                newName: "IX_Votos_UsuarioId");

            migrationBuilder.RenameIndex(
                name: "IX_voto_RestauranteId",
                table: "Votos",
                newName: "IX_Votos_RestauranteId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Votos",
                table: "Votos",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Votos_Restaurantes_RestauranteId",
                table: "Votos",
                column: "RestauranteId",
                principalTable: "Restaurantes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Votos_Usuarios_UsuarioId",
                table: "Votos",
                column: "UsuarioId",
                principalTable: "Usuarios",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
