﻿using Dapper;
using Npgsql;
using OndeComer.ClassLibrary.models;
using OndeComer.ClassLibrary.repositorios;
using OndeComer.InfraDapper.Configuration;
using OndeComer.InfraDapper.Sql;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OndeComer.InfraDapper.Repositories
{
    public class RestauranteDapperRepository : IRestauranteRepositorio
    {
        private readonly PostgresConfiguration _postgresConfiguration;

        public RestauranteDapperRepository(PostgresConfiguration postgresConfiguration)
        {
            _postgresConfiguration = postgresConfiguration;
        }

        public void AtualizarRestaurante(Restaurante restaurante)
        {
            throw new System.NotImplementedException();
        }

        public async void InserirRestaurante(Restaurante restaurante)
        {
            using (var db = new NpgsqlConnection(_postgresConfiguration.ConnectionString))
            {
                await db.ExecuteAsync(SqlResourceExtension.InserirResutaurante,
                    new
                    {
                        Nome = restaurante.Nome
                    });
            }
        }

        public async Task<List<Restaurante>> ListarRestaurante()
        {
            using (var db = new NpgsqlConnection(_postgresConfiguration.ConnectionString))
            {
                var result = await db.QueryAsync<Restaurante>(SqlResourceExtension.ListarRestaurantes);

                return result.ToList();
            }
        }

        public Task<Restaurante> ObterInteralRestaurante(long idRestaurante)
        {
            throw new System.NotImplementedException();
        }

        public Task<Restaurante> ObterRestaurante(long idRestaurante)
        {
            throw new System.NotImplementedException();
        }
    }
}
