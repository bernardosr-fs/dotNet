﻿using OndeComer.ClassLibrary.Mappers;
using OndeComer.ClassLibrary.models.CrescerApiAuth;
using OndeComer.Infra.ExternalService.Dtos;
using OndeComer.Infra.ExternalService.Services;
using System.Threading.Tasks;

namespace OndeComer.ClassLibrary.services
{
    public class AutenticacaoService
    {
        private readonly ICrescerApiAuth _crescerApiAuth;

        public AutenticacaoService(ICrescerApiAuth crescerApiAuth)
        {
            _crescerApiAuth = crescerApiAuth;
        }

        public async Task<LoginResponseDto> Login(Login login)
        {
            return await _crescerApiAuth.Login(login.Map());
        }

        public async Task<UsuarioResponseDto> ObterUsuario()
        {
            return await _crescerApiAuth.ObterUsuario();
        }

        public async Task<RegistrarResponseDto> Registrar(Registrar registrar)
        {
            return await _crescerApiAuth.Registrar(registrar.Map());
        }

        public async Task<StatusReponseDto> ObterStatus()
        {
            return await _crescerApiAuth.Status();
        }

        public async Task Deletar()
        {
            await _crescerApiAuth.Deletar();
        }

        public async Task Logout()
        {
            await _crescerApiAuth.Logout();
        }
    }
}
