﻿using Microsoft.Extensions.Options;
using OndeComer.ClassLibrary.models;
using OndeComer.ClassLibrary.repositorios;
using OndeComer.ClassLibrary.settings;
using System;
using System.Threading.Tasks;

namespace OndeComer.ClassLibrary.services
{
    public class VotacaoService
    {
        private readonly TimeSpan _tempoLimiteVotacao;
        private readonly IUsuarioRepositorio _usuarioRepositorio;
        private readonly IRestauranteRepositorio _restauranteRepositorio;
        private readonly IVotacaoRepositorio _votacaoRepositorio;

        public VotacaoService(IUsuarioRepositorio usuarioRepositorio,
                                IRestauranteRepositorio restauranteRepositorio,
                                IVotacaoRepositorio votacaoRepositorio,
                                IOptionsMonitor<VotacaoSettings> votacaoSettings)
        {
            _usuarioRepositorio = usuarioRepositorio;
            _restauranteRepositorio = restauranteRepositorio;
            _votacaoRepositorio = votacaoRepositorio;

            _tempoLimiteVotacao = votacaoSettings.CurrentValue.HoraLimiteVotacao;
        }

        public async Task<Votacao> ObterSessaoVotacao()
        {
            var votacao = await _votacaoRepositorio.ObterSessaoVotacaoDapper();

            return votacao;
        }

        public async Task RealizarVotacao(int idUsuario, int idRestaurante)
        {
            if (DateTime.Now.Hour > _tempoLimiteVotacao.Hours && DateTime.Now.Minute > _tempoLimiteVotacao.Minutes)
                throw new Exception("Horário limite para votação atingido");

            var usuario = await _usuarioRepositorio.ObterUsuario(idUsuario);

            if (usuario == null)
                throw new Exception("Usuário não existe");

            var restaurante = await _restauranteRepositorio.ObterRestaurante(idRestaurante);

            if (restaurante == null)
                throw new Exception("Restaurante não encontrado");

            var votosUsuario = await _votacaoRepositorio.ObterVotoPorUsuarioId(idUsuario);

            if (votosUsuario != null)
                throw new Exception("Usuário já votou");

            var voto = new Voto(usuario, restaurante);

            _votacaoRepositorio.InserirVoto(voto);
        }
    }
}
