﻿using FluentValidation;

namespace OndeComer.ClassLibrary.Dtos.Validators
{
    public class RestauranteDtoValidator : AbstractValidator<RestauranteDto>
    {
        public RestauranteDtoValidator()
        {
            RuleFor(x => x.Nome).NotEmpty().NotEqual("Teste").WithMessage("Por favor inserir seu nome");
            RuleFor(x => x.Endereco).NotNull().WithMessage("Por favor inserir o endereço");            
            RuleFor(x => x.Endereco.Rua).NotNull().When(x => x.Endereco != null).WithMessage("Por favor inserir a rua");
        }
    }
}
