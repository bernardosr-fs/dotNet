﻿using OndeComer.ClassLibrary.models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OndeComer.ClassLibrary.repositorios
{
    public interface IRestauranteRepositorio
    {
        Task<Restaurante> ObterInteralRestaurante(long idRestaurante);
        Task<Restaurante> ObterRestaurante(long idRestaurante);
        Task<List<Restaurante>> ListarRestaurante();
        void AtualizarRestaurante(Restaurante restaurante);
        void InserirRestaurante(Restaurante restaurante);
    }
}
