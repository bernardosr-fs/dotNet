﻿using System;

namespace OndeComer.ClassLibrary.models
{
    public class Usuario
    {
        public long Id { get; private set; }
        public string Nome { get; private set; }

        public Usuario()
        {

        }

        public Usuario(int id, string nome)
        {
            Id = id;
            Nome = nome;
        }

        internal void Atualizar(Usuario usuario)
        {
            Nome = usuario.Nome;
        }
    }
}
