﻿namespace OndeComer.ClassLibrary.models.CrescerApiAuth
{
    public class Registrar
    {
        public Registrar(string email, string nome, string sobrenome, string senha, string[] permissoes)
        {
            Email = email;
            Nome = nome;
            Sobrenome = sobrenome;
            Senha = senha;
            Permissoes = permissoes;
        }

        public string Email { get; }

        public string Nome { get; }

        public string Sobrenome { get; }

        public string Senha { get; }

        public string[] Permissoes { get; }
    }
}
