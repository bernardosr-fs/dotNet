﻿namespace OndeComer.ClassLibrary.models.CrescerApiAuth
{
    public class Token
    {
        public Token(string valor)
        {
            Valor = valor;
        }

        public string Valor { get; }
    }
}
