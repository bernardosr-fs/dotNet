﻿using OndeComer.WebApi.Requests;
using System.ComponentModel.DataAnnotations;

namespace OndeComer.WebApi.Attributes
{
    public class UsuarioValidacaoAtributo : ValidationAttribute
    {
        public override bool IsValid(object? value)
        {
            var usuarioRequest = (InsereUsuarioRequest)value;

            return usuarioRequest.Id > 0 && !string.IsNullOrEmpty(usuarioRequest.Nome);

        }
    }
}
