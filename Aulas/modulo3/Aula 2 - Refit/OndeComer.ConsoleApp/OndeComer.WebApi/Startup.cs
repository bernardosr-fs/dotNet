using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using OndeComer.ClassLibrary.Dtos.Validators;
using OndeComer.ClassLibrary.repositorios;
using OndeComer.ClassLibrary.services;
using OndeComer.ClassLibrary.settings;
using OndeComer.Infra.Context;
using OndeComer.Infra.ExternalService.Services;
using OndeComer.Infra.RepositoriosPostgres;
using OndeComer.InfraDapper.Configuration;
using OndeComer.InfraDapper.Repositories;
using OndeComer.WebApi.Config;
using Refit;
using System;

namespace OndeComer.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "OndeComer.WebApi", Version = "v1" });
            });

            services.AddScoped<IUsuarioRepositorio, UsuarioRepositorioPostgres>();
            services.AddScoped<IRestauranteRepositorio, RestauranteRepositorioPostgres>();
            services.AddScoped<IVotacaoRepositorio, VotacaoDapperRepository>();
            services.AddScoped<VotacaoService>();
            services.AddScoped<RestauranteService>();
            services.AddScoped<AutenticacaoService>();

            services.Configure<ArquivosSettings>(Configuration.GetSection("Arquivos"));
            services.Configure<VotacaoSettings>(Configuration.GetSection("Votacao"));

            var postgreSqlConfiguration = Configuration.GetSection("NpgsqlConnection").Get<PostgresConfiguration>();

            services.AddSingleton(postgreSqlConfiguration);

            services.AddDbContext<OndeComerDbContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));

            services.AddSingleton<RestauranteDtoValidator>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddRefitClient<ICrescerApiAuth>()
               .ConfigureHttpClient(client => client.BaseAddress = new Uri("https://crescer-api-auth.herokuapp.com"))
               .AddHttpMessageHandler<AuthHeaderHandler>();

            services.AddTransient<AuthHeaderHandler>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "OndeComer.WebApi v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseMiddleware<ExceptionMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
