﻿using System.ComponentModel.DataAnnotations;

namespace OndeComer.WebApi.Requests
{
    public class InsereUsuarioRequest
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}
