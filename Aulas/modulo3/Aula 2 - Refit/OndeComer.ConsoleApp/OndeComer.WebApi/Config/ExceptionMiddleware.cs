﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Refit;
using System;
using System.Threading.Tasks;

namespace OndeComer.WebApi.Config
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (ApiException ex)
            {
                await httpContext.Response.WriteAsJsonAsync($"Exception Message {ex.Message} Content:{ex.Content}");
                httpContext.Response.StatusCode = (int)ex.StatusCode;
            }
            catch (Exception ex)
            {
                await httpContext.Response.WriteAsJsonAsync("Houve um erro na aplicação");
            }
        }
    }
}
