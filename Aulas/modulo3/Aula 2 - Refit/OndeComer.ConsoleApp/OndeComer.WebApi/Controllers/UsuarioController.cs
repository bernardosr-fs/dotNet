﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OndeComer.ClassLibrary.models;
using OndeComer.ClassLibrary.repositorios;
using OndeComer.ClassLibrary.services;
using OndeComer.WebApi.Mappers;
using OndeComer.WebApi.Requests;
using OndeComer.WebApi.Responses;
using System.Threading.Tasks;

namespace OndeComer.WebApi.Controllers
{
    [ApiController]
    [Route("usuario")]
    public class UsuarioController : Controller
    {
        private readonly UsuarioService _usuarioService;

        public UsuarioController(UsuarioService usuarioService)
        {
            _usuarioService = usuarioService;
        }

        [HttpGet("obter-usuario/{idUsuario}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Usuario>> ObterUsuario(int idUsuario)
        {
            var usuario = await _usuarioService.ObterUsuario(idUsuario);

            return Ok(usuario);
        }

        [HttpGet("obter-usuario-resumido/{idUsuario}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<UsuarioResponse>> ObterUsuarioResumido(int idUsuario)
        {
            var usuario = await _usuarioService.ObterUsuario(idUsuario);

            return Ok(usuario.Map());
        }

        [HttpPost("inserir-usuario")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> InserirUsuario(InsereUsuarioRequest usuarioRequest)
        {
            try
            {
                await _usuarioService.InserirUsuario(usuarioRequest.Map());

                return Ok();
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
