﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OndeComer.ClassLibrary.models;
using OndeComer.ClassLibrary.repositorios;
using OndeComer.ClassLibrary.services;
using OndeComer.WebApi.Requests;
using System;
using System.Threading.Tasks;

namespace OndeComer.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VotacaoController : Controller
    {
        private readonly VotacaoService _votacaoService;

        public VotacaoController(VotacaoService votacaoService)
        {
            _votacaoService = votacaoService;
        }

        [HttpGet("obter-sessao-votacao")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Votacao>> ObterSessaoVotacao()
        {
            var sessaoVotacao = await _votacaoService.ObterSessaoVotacao();

            return Ok(sessaoVotacao);
        }

        [HttpPost("realizar-votacao")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> RealizarVotacao(RealizaVotacaoRequest realizaVotacaoRequest)
        {
            try
            {
                await _votacaoService.RealizarVotacao(realizaVotacaoRequest.IdUsuario, realizaVotacaoRequest.IdRestaurante);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
