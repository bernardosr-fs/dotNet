﻿using OndeComer.ClassLibrary.models.CrescerApiAuth;
using OndeComer.WebApi.Requests.CrescerApiAuth;

namespace OndeComer.WebApi.Mappers
{
    public static class AutenticaoDtoMapper
    {
        public static Login Map(this LoginRequest request)
        {
            return new Login(request.Email, request.Senha);
        }

        public static Registrar Map(this RegistrarRequest request)
        {
            return new Registrar(request.Email, request.Nome, request.Sobrenome, request.Senha, request.Permissoes);
        }
    }
}
