using Microsoft.Extensions.Options;
using OndeComer.ClassLibrary.repositorios;
using OndeComer.ClassLibrary.services;
using OndeComer.ClassLibrary.settings;
using System;
using System.Threading.Tasks;
using Xunit;

namespace OndeComer.ConsoleApp.Tests
{
    public class VotacaoServiceTest
    {
        //[Fact]
        public async Task DadoUsuarioParaVotar_QuandoNaoExisteNaBase_EntaoDeveRetornarExcecao()
        {
            ////Arrange
            //var usuarioRepositorio = new UsuarioRepositorioCsv();
            //var restauranteRepositorio = new RestauranteRepositorioCsv();
            //var votacaoRepositorio = new VotacaoRepositorioCsv(usuarioRepositorio, restauranteRepositorio);
            //var votacaoService = new VotacaoService(usuarioRepositorio, restauranteRepositorio, votacaoRepositorio);

            ////Act
            //var result = await Assert.ThrowsAsync<Exception>(() => votacaoService.RealizarVotacao(120, 1));

            ////Assert
            //Assert.Equal("Usu�rio n�o existe", result.Message);
        }

        //[Theory]
        //[InlineData(121, 333)]
        //[InlineData(122, 333)]
        //[InlineData(123, 333)]
        //[InlineData(124, 333)]
        //[InlineData(125, 333)]
        public async Task DadoUsuariosParaVotar_QuandoNaoExistemNaBase_EntaoDeveRetornarExcecao(int idUsuario, int idRestaurante)
        {
            ////Arrange
            //var usuarioRepositorio = new UsuarioRepositorioCsv();
            //var restauranteRepositorio = new RestauranteRepositorioCsv();
            //var votacaoRepositorio = new VotacaoRepositorioCsv(usuarioRepositorio, restauranteRepositorio);
            //var votacaoService = new VotacaoService(usuarioRepositorio, restauranteRepositorio, votacaoRepositorio);

            ////Act
            //var result = await Assert.ThrowsAsync<Exception>(() => votacaoService.RealizarVotacao(idUsuario, idRestaurante));

            ////Assert
            //Assert.Equal("Usu�rio n�o existe", result.Message);
        }
    }
}
