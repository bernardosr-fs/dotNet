﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OndeComer.InfraDapper.Configuration
{
    public class PostgresConfiguration
    {
        public string ConnectionString { get; set; }
    }
}
