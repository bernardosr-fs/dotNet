﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OndeComer.Infra.ExternalService.Dtos;
using OndeComer.Infra.ExternalService.Services;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace OndeComer.WebApi.Config
{
    public class CrescerApiAuthentication : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private readonly ICrescerApiAuth _crescerApiAuth;

        public CrescerApiAuthentication(
            IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            ICrescerApiAuth crescerApiAuth) : base(options, logger, encoder, clock)
        {
            _crescerApiAuth = crescerApiAuth;
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            var context = Context.GetEndpoint();
            if (context?.Metadata.GetMetadata<IAllowAnonymous>() != null)
                return AuthenticateResult.NoResult();

            if (!Request.Headers.ContainsKey("Authorization"))
                return AuthenticateResult.Fail("Falta o Authorization Header");

            UsuarioResponseDto usuario;
            try
            {
                usuario = await _crescerApiAuth.ObterUsuario();
            }
            catch
            {
                return AuthenticateResult.Fail("Email ou Senha Incorretos");
            }

            var claims = new List<Claim>();

            foreach (var roleValue in usuario.Roles)
                claims.Add(new Claim(ClaimTypes.Role, roleValue));

            var identity = new ClaimsIdentity(claims, Scheme.Name);
            var principal = new ClaimsPrincipal(identity);

            var ticket = new AuthenticationTicket(principal, Scheme.Name);

            return AuthenticateResult.Success(ticket);
        }
    }
}
