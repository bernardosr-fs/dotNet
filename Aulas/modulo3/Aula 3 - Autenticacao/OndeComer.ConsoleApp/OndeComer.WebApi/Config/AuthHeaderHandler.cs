﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace OndeComer.WebApi.Config
{
    public class AuthHeaderHandler : DelegatingHandler
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AuthHeaderHandler(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage requestRefit,
            CancellationToken cancellationToken)
        {
            var requestOnderComer = _httpContextAccessor.HttpContext.Request;

            var tokenHeader = requestOnderComer.Headers["Authorization"];

            if (!string.IsNullOrEmpty(tokenHeader))
            {
                requestRefit.Headers.Add("Authorization", new List<string>() { tokenHeader });
            }

            return await base.SendAsync(requestRefit, cancellationToken);
        }
    }
}
