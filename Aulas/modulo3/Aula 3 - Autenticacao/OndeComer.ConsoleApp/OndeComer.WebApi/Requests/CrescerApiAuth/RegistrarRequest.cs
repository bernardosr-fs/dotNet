﻿namespace OndeComer.WebApi.Requests.CrescerApiAuth
{
    public class RegistrarRequest
    {
        public string Email { get; set; }

        public string Nome { get; set; }

        public string Sobrenome { get; set; }

        public string Senha { get; set; }

        public string[] Permissoes { get; set; }
    }
}
