﻿using OndeComer.ClassLibrary.models;
using OndeComer.WebApi.Responses;

namespace OndeComer.WebApi.Mappers
{
    public static class UsuarioResponseMapper
    {
        public static UsuarioResponse Map(this Usuario usuario)
        {
            return new UsuarioResponse
            {
                Nome = usuario.Nome
            };
        }
    }
}
