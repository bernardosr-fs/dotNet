﻿using OndeComer.ClassLibrary.Dtos;
using OndeComer.ClassLibrary.models;
using OndeComer.WebApi.Requests;

namespace OndeComer.WebApi.Mappers
{
    public static class RestauranteRequestMapper
    {
        public static RestauranteDto Map(this InsereRestauranteRequest insereUsuarioRequest)
        {
            return new RestauranteDto { Nome = insereUsuarioRequest.Nome };
        }

        public static RestauranteDto Map(this AtualizarRestauranteRequest atualizarUsuarioRequest)
        {
            return new RestauranteDto { Id = atualizarUsuarioRequest.Id, Nome = atualizarUsuarioRequest.Nome };
        }
    }
}
