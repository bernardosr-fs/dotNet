﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OndeComer.ClassLibrary.services;
using OndeComer.WebApi.Mappers;
using OndeComer.WebApi.Requests.CrescerApiAuth;
using System.Threading.Tasks;

namespace OndeComer.WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class AutenticaoCrescerApiController : Controller
    {
        private readonly AutenticacaoService _autenticacaoService;

        public AutenticaoCrescerApiController(AutenticacaoService autenticacaoService)
        {
            _autenticacaoService = autenticacaoService;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginRequest request)
        {
            return Ok(await _autenticacaoService.Login(request.Map()));
        }

        [AllowAnonymous]
        [HttpPost("Registrar")]
        public async Task<IActionResult> Registrar(RegistrarRequest request)
        {
            return Ok(await _autenticacaoService.Registrar(request.Map()));
        }

        [HttpPost("Logout")]
        public async Task<IActionResult> Logout()
        {
            await _autenticacaoService.Logout();
            return Ok();
        }


        [HttpGet("ObterUsuario")]
        public async Task<IActionResult> ObterUsuario()
        {
            return Ok(await _autenticacaoService.ObterUsuario());
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("ObterStatus")]
        public async Task<IActionResult> ObterStatus()
        {
            return Ok(await _autenticacaoService.ObterStatus());
        }

        [HttpDelete("Deletar")]
        public async Task<IActionResult> Deletar()
        {
            await _autenticacaoService.Deletar();

            return Ok();
        }
    }
}
