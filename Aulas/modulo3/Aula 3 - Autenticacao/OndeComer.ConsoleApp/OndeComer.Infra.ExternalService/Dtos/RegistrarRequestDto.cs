﻿using Newtonsoft.Json;

namespace OndeComer.Infra.ExternalService.Dtos
{
    public class RegistrarRequestDto
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("firtName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("roles")]
        public string[] Roles { get; set; }
    }
}
