﻿namespace OndeComer.Infra.ExternalService.Dtos
{
    public class UsuarioResponseDto
    {
        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string[] Roles { get; set; }
    }
}
