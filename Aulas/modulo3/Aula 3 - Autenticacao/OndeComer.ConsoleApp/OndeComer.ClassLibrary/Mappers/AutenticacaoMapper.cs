﻿using OndeComer.ClassLibrary.models.CrescerApiAuth;
using OndeComer.Infra.ExternalService.Dtos;

namespace OndeComer.ClassLibrary.Mappers
{
    public static class AutenticacaoMapper
    {
        public static LoginRequestDto Map(this Login login)
        {
            return new LoginRequestDto()
            {
                Email = login.Email,
                Password = login.Senha
            };
        }

        public static RegistrarRequestDto Map(this Registrar login)
        {
            return new RegistrarRequestDto()
            {
                Email = login.Email,
                Password = login.Senha,
                FirstName = login.Nome,
                LastName = login.Sobrenome,
                Roles = login.Permissoes
            };
        }
    }
}
