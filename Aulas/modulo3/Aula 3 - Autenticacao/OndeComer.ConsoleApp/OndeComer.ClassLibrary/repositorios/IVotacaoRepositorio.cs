﻿
using OndeComer.ClassLibrary.models;
using System.Threading.Tasks;

namespace OndeComer.ClassLibrary.repositorios
{
    public interface IVotacaoRepositorio
    {
        Votacao ObterSessaoVotacao();
        Task<Votacao> ObterSessaoVotacaoDapper();
        Task<Voto> ObterVotoPorUsuarioId(long usuarioId);
        void InserirVoto(Voto voto);
        void AlterarVotacao(Votacao votacao);
    }
}
