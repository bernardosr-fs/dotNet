﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace OndeComer.ClassLibrary.models
{
    public class Restaurante
    {
        public long Id { get; }
        public string Nome { get; private set; }

        protected Restaurante() { }

        public Restaurante(long id, string nome)
        {
            Id = id;
            Nome = nome;
        }

        public Restaurante(string nome)
        {
            Nome = nome;
        }

        public class RestauranteComparer : IEqualityComparer<Restaurante>
        {
            public bool Equals(Restaurante x, Restaurante y)
            {
                return x.Id == y.Id;
            }

            public int GetHashCode([DisallowNull] Restaurante obj)
            {
                if (obj == null)
                    return 0;

                return (int)obj.Id;
            }
        }

        internal void Atualizar(Restaurante restaurante)
        {
            Nome = restaurante.Nome;
        }
    }
}
