﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OndeComer.ClassLibrary.models;
using OndeComer.ClassLibrary.services;
using OndeComer.WebApi.Mappers;
using OndeComer.WebApi.Requests;
using System;
using System.Threading.Tasks;

namespace OndeComer.WebApi.Controllers
{
    [ApiController]
    [Route("restaurante")]
    public class RestauranteController : Controller
    {
        private readonly RestauranteService _restauranteService;

        public RestauranteController(RestauranteService restauranteService)
        {
            _restauranteService = restauranteService;
        }

        [HttpGet("obter-restaurante/{idRestaurante}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Restaurante>> ObterRestaurante(int idRestaurante)
        {
            var restaurante = await _restauranteService.ObterRestaurante(idRestaurante);

            return Ok(restaurante);
        }

        [HttpGet("listar-restaurante")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Restaurante>> ListarRestaurante()
        {
            var restaurante = await _restauranteService.ListarRestaurante();

            return Ok(restaurante);
        }

        [HttpPut("atualizar-restaurante/{idRestaurante}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Restaurante>> AtualizarRestaurante(long idRestaurante, AtualizarRestauranteRequest restauranteRequest)
        {
            restauranteRequest.Id = idRestaurante;
            await _restauranteService.AtualizarRestaurante(restauranteRequest.Map());

            return Ok();
        }

        [HttpPost("inserir-restaurante")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> InserirRestaurante(InsereRestauranteRequest restauranteRequest)
        {
            try
            {
                _restauranteService.InserirRestaurante(restauranteRequest.Map());

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
