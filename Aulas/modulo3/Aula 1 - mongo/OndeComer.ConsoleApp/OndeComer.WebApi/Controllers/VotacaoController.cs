﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OndeComer.ClassLibrary.models;
using OndeComer.ClassLibrary.repositorios;
using OndeComer.ClassLibrary.services;
using OndeComer.WebApi.Requests;
using System;
using System.Threading.Tasks;

namespace OndeComer.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VotacaoController : Controller
    {
        private readonly VotacaoService _votacaoService;
        private readonly IVotacaoRepositorio _votacaoRepositorio;

        public VotacaoController(VotacaoService votacaoService, IVotacaoRepositorio votacaoRepositorio)
        {
            _votacaoService = votacaoService;
            _votacaoRepositorio = votacaoRepositorio;
        }

        [HttpGet("obter-sessao-votacao")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Votacao>> ObterSessaoVotacao()
        {
            var sessaoVotacao = await _votacaoRepositorio.ObterSessaoVotacao();

            return Ok(sessaoVotacao);
        }

        [HttpPost("realizar-votacao")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> RealizarVotacao(RealizaVotacaoRequest realizaVotacaoRequest)
        {
            try
            {
                await _votacaoService.RealizarVotacao(realizaVotacaoRequest.IdUsuario, realizaVotacaoRequest.IdRestaurante);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost()]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> Inserir(RealizaVotacaoRequest realizaVotacaoRequest)
        {
            try
            {
                bool isMongoDb = true;
                await _votacaoService.RealizarVotacao(realizaVotacaoRequest.IdUsuario, realizaVotacaoRequest.IdRestaurante, isMongoDb);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet()]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> ObterVotacao()
        {
            try
            {
                var response = await _votacaoService.BuscarVotos();
                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
