﻿using System.ComponentModel.DataAnnotations;

namespace OndeComer.WebApi.Requests
{
    public class InsereRestauranteRequest
    {
        public string Nome { get; set; }
    }
}
