﻿namespace OndeComer.WebApi.Requests
{
    public class AtualizarRestauranteRequest
    {
        public long Id { get; set; }
        public string Nome { get; set; }
    }
}
