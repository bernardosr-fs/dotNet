﻿using System.ComponentModel.DataAnnotations;

namespace OndeComer.WebApi.Requests
{
    public class InsereUsuarioRequest
    {
        [Required(ErrorMessage = "Id é obrigatório")]
        public int Id { get; set; }
        [Required]
        [StringLength(10, ErrorMessage = "O nome excede a quantidade de caracteres permitidos")]
        public string Nome { get; set; }
    }
}
