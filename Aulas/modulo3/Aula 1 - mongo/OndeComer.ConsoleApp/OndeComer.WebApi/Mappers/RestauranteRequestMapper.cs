﻿using OndeComer.ClassLibrary.models;
using OndeComer.WebApi.Requests;

namespace OndeComer.WebApi.Mappers
{
    public static class UsuariRestauranteRequestMapperoRequestMapper
    {
        public static Restaurante Map(this InsereRestauranteRequest insereUsuarioRequest)
        {
            return new Restaurante(insereUsuarioRequest.Nome);
        }

        public static Restaurante Map(this AtualizarRestauranteRequest atualizarUsuarioRequest)
        {
            return new Restaurante(atualizarUsuarioRequest.Id,
                                    atualizarUsuarioRequest.Nome);
        }
    }
}
