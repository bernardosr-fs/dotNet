﻿
using OndeComer.ClassLibrary.models;
using System.Threading.Tasks;

namespace OndeComer.ClassLibrary.repositorios
{
    public interface IVotacaoRepositorio
    {
        Task<Votacao> ObterSessaoVotacao();
        Task SalvarVotos(Votacao votacao);
    }
}
