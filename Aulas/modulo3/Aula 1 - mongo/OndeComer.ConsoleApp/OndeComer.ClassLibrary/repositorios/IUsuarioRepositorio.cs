﻿using OndeComer.ClassLibrary.models;
using System.Threading.Tasks;

namespace OndeComer.ClassLibrary.repositorios
{
    public interface IUsuarioRepositorio
    {
        Task<Usuario> ObterUsuario(int idUsuario);
        Task InserirUsuario(Usuario usuario);
    }
}
