﻿using OndeComer.ClassLibrary.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OndeComer.ClassLibrary.repositorios
{
    public interface IVotacaoRepositorioMongo
    {
        public Task Inserir(Votacao votacao);
        public Task<List<Votacao>> Listar();
    }
}
