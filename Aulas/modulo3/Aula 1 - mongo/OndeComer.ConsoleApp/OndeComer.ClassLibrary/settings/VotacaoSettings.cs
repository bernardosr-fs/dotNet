﻿using System;
using System.ComponentModel.DataAnnotations;

namespace OndeComer.ClassLibrary.settings
{
    public class VotacaoSettings
    {
        [HoraLimiteVotacaoValidator]
        public TimeSpan HoraLimiteVotacao { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class HoraLimiteVotacaoValidatorAttribute : ValidationAttribute
    {
        public HoraLimiteVotacaoValidatorAttribute()
        {
            ErrorMessage = "Hora com formato inválido";
        }

        public override bool IsValid(object? value)
        {
            var horaLimiteVotacao = value?.ToString();
            var horas = horaLimiteVotacao.Split(":")[0];
            var minutos = horaLimiteVotacao.Split(":")[1];

            try
            {
                var timespan = new TimeSpan(int.Parse(horas), int.Parse(minutos), 0);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
