﻿namespace OndeComer.ClassLibrary.models
{
    public class Voto
    {
        public Usuario Usuario { get; set; }
        public Restaurante Restaurante { get; set; }

        public Voto()
        {

        }

        public Voto(Usuario usuario, Restaurante restaurante)
        {
            Usuario = usuario;
            Restaurante = restaurante;
        }
    }
}