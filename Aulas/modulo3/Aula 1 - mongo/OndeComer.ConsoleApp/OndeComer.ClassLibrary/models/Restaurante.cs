﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace OndeComer.ClassLibrary.models
{
    public class Restaurante
    {
        [Key, Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long IdRestaurante { get; set; }

        [Key, Column(Order = 1)]
        public string Nome { get; set; }

        protected Restaurante() { }

        public Restaurante(long id, string nome)
        {
            IdRestaurante = id;
            Nome = nome;
        }

        public Restaurante(string nome)
        {
            Nome = nome;
        }

        public class RestauranteComparer : IEqualityComparer<Restaurante>
        {
            public bool Equals(Restaurante x, Restaurante y)
            {
                return x.IdRestaurante == y.IdRestaurante;
            }

            public int GetHashCode([DisallowNull] Restaurante obj)
            {
                if (obj == null)
                    return 0;

                return (int)obj.IdRestaurante;
            }
        }

        internal void Atualizar(Restaurante restaurante)
        {
            Nome = restaurante.Nome;
        }
    }
}
