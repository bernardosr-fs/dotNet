﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OndeComer.ClassLibrary.models
{
    public class Votacao
    {
        public ObjectId? Id { get; set; }
        public DateTime Data { get; set; }
        public List<Voto> Votos { get; set; }

        public Votacao()
        {
                
        }

        public Votacao(DateTime data) : this(data, null)
        { }

        public Votacao(DateTime data, List<Voto> votos)
        {
            Data = data;
            Votos = votos ?? new List<Voto>();
        }

        public void AtribuirVoto(Voto voto)
        {
            if (Votos.Exists(x => x.Usuario.IdUsuario == voto.Usuario.IdUsuario))
                throw new Exception("Este usuário já votou");

            Votos.Add(voto);
        }

        public void ImprimirVotos()
        {
            var restaurantesVotados = Votos.Select(x => x.Restaurante)
                                            .Distinct(new Restaurante.RestauranteComparer());

            Console.WriteLine($"Inicio Resultado");

            foreach (var restauranteVotado in restaurantesVotados)
                Console.WriteLine($"Restaurante: {restauranteVotado.Nome}\nQtd Votos: {Votos.Count(x => x.Restaurante.Nome == restauranteVotado.Nome)}");

            Console.WriteLine($"Fim Resultado");
        }
    }
}
