﻿using MongoDB.Bson.Serialization.Attributes;

namespace OndeComer.ClassLibrary.models
{
    public class Usuario
    {
        public int IdUsuario { get; set; }
        public string Nome { get; set; }

        public Usuario()
        {

        }

        public Usuario(int id, string nome)
        {
            IdUsuario = id;
            Nome = nome;
        }
    }
}
