﻿using Microsoft.Extensions.Options;
using OndeComer.ClassLibrary.models;
using OndeComer.ClassLibrary.repositorios;
using OndeComer.ClassLibrary.settings;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OndeComer.ClassLibrary.services
{
    public class VotacaoService
    {
        private readonly TimeSpan _tempoLimiteVotacao;
        private readonly IUsuarioRepositorio _usuarioRepositorio;
        private readonly IRestauranteRepositorio _restauranteRepositorio;
        private readonly IVotacaoRepositorio _votacaoRepositorio;
        private readonly IVotacaoRepositorioMongo _votacaoRepositorioMongo;

        public VotacaoService(IUsuarioRepositorio usuarioRepositorio,
                                IRestauranteRepositorio restauranteRepositorio,
                                IVotacaoRepositorio votacaoRepositorio,
                                IOptionsMonitor<VotacaoSettings> votacaoSettings,
                                IVotacaoRepositorioMongo votacaoRepositorioMongo)
        {
            _usuarioRepositorio = usuarioRepositorio;
            _restauranteRepositorio = restauranteRepositorio;
            _votacaoRepositorio = votacaoRepositorio;
            _votacaoRepositorioMongo = votacaoRepositorioMongo;

            _tempoLimiteVotacao = votacaoSettings.CurrentValue.HoraLimiteVotacao;
        }

        public async Task RealizarVotacao(int idUsuario, int idRestaurante, bool isMongoDB = false)
        {
            if (DateTime.Now.Hour > _tempoLimiteVotacao.Hours && DateTime.Now.Minute > _tempoLimiteVotacao.Minutes)
                throw new Exception("Horário limite para votação atingido");

            var usuario = await _usuarioRepositorio.ObterUsuario(idUsuario);

            if (usuario == null)
                throw new Exception("Usuário não existe");

            var restaurante = await _restauranteRepositorio.ObterRestaurante(idRestaurante);

            if (restaurante == null)
                throw new Exception("Restaurante não encontrado");

            var sessaoVotacao = await _votacaoRepositorio.ObterSessaoVotacao();

            var voto = new Voto(usuario, restaurante);
            sessaoVotacao.AtribuirVoto(voto);

            if (isMongoDB)
              await _votacaoRepositorioMongo.Inserir(sessaoVotacao);
            else
              await _votacaoRepositorio.SalvarVotos(sessaoVotacao);

            sessaoVotacao.ImprimirVotos();
        }

        public async Task<List<Votacao>> BuscarVotos()
        {
           return await _votacaoRepositorioMongo.Listar();
        }
    }
}
