﻿using OndeComer.ClassLibrary.models;
using OndeComer.ClassLibrary.repositorios;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OndeComer.ClassLibrary.services
{
    public class RestauranteService
    {
        private readonly IRestauranteRepositorio _restauranteRepositorio;

        public RestauranteService(IRestauranteRepositorio restauranteRepositorio)
        {
            _restauranteRepositorio = restauranteRepositorio;
        }

        public async Task<bool> InserirRestaurante(Restaurante restaurante)
        {
            _restauranteRepositorio.InserirRestaurante(restaurante);

            return true;
        }

        public async Task<Restaurante> ObterRestaurante(int idRestaurante)
        {
            return await _restauranteRepositorio.ObterRestaurante(idRestaurante);
        }

        public async Task<List<Restaurante>> ListarRestaurante()
        {
            return await _restauranteRepositorio.ListarRestaurante();
        }

        public async Task AtualizarRestaurante(Restaurante restaurante)
        {
            var restauranteASerAtualizado = await _restauranteRepositorio.ObterInteralRestaurante(restaurante.IdRestaurante);

            if (restauranteASerAtualizado == null)
                return;

            restauranteASerAtualizado.Atualizar(restaurante);

            _restauranteRepositorio.AtualizarRestaurante(restauranteASerAtualizado);
        }
    }
}
