﻿using Microsoft.Extensions.Options;
using OndeComer.ClassLibrary.models;
using OndeComer.ClassLibrary.repositorios;
using OndeComer.ClassLibrary.settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OndeComer.Infra.Repositorios
{
    public class VotacaoRepositorioCsv : IVotacaoRepositorio
    {
        private readonly string CaminhoCsvVotacoes;
        private readonly string CaminhoCsvVotos;
        private readonly IUsuarioRepositorio _usuarioRepositorio;
        private readonly IRestauranteRepositorio _restauranteRepositorio;

        public VotacaoRepositorioCsv(IUsuarioRepositorio usuarioRepositorio, IRestauranteRepositorio restauranteRepositorio, IOptionsMonitor<ArquivosSettings> arquivosSettings)
        {
            _usuarioRepositorio = usuarioRepositorio;
            _restauranteRepositorio = restauranteRepositorio;

            CaminhoCsvVotacoes = arquivosSettings.CurrentValue.VotacoesCsvPath;
            CaminhoCsvVotos = arquivosSettings.CurrentValue.VotosCsvPath;
        }

        public async Task<Votacao> ObterSessaoVotacao()
        {
            var linhas = await File.ReadAllLinesAsync(CaminhoCsvVotacoes);

            var linhaSessaoVotacao = linhas?.FirstOrDefault(x =>
            {
                var colunas = x.Split(',');
                var dataVotacao = DateTime.Parse(colunas[0]);

                return dataVotacao == DateTime.Now.Date;
            });

            if (linhaSessaoVotacao == null)
            {
                await AdicionarVotacao();
                return new Votacao(DateTime.Now);
            }
            else
            {
                var dataVotacao = DateTime.Parse(linhaSessaoVotacao.Split(',')[0]);
                var votos = await ObterVotos(dataVotacao);

                return new Votacao(dataVotacao, votos);
            }
        }

        public async Task SalvarVotos(Votacao votacao)
        {
            var stringBuilder = new StringBuilder();

            var linhas = await File.ReadAllLinesAsync(CaminhoCsvVotos);

            foreach (var linha in linhas)
            {
                var colunas = linha.Split(',');
                var dataVotacaoCol = DateTime.Parse(colunas[0]);

                if (dataVotacaoCol.Date != votacao.Data.Date)
                    stringBuilder.AppendLine(linha);
                else
                    continue;
            }

            foreach (var voto in votacao.Votos)
                stringBuilder.AppendLine($"{votacao.Data.ToString("yyyy-MM-dd")},{voto.Usuario.IdUsuario},{voto.Restaurante.IdRestaurante}");

            await File.WriteAllTextAsync(CaminhoCsvVotos, stringBuilder.ToString());
        }

        private async Task AdicionarVotacao()
        {
            var data = DateTime.Now.ToString("yyyy-MM-dd");

            await File.AppendAllLinesAsync(CaminhoCsvVotacoes, new List<string> { data });
        }

        private async Task<List<Voto>> ObterVotos(DateTime dataVotacao)
        {
            var linhas = await File.ReadAllLinesAsync(CaminhoCsvVotos);
            var votos = new List<Voto>();

            foreach (var linha in linhas)
            {
                var colunas = linha.Split(',');
                var dataVotacaoCol = DateTime.Parse(colunas[0]);

                if (dataVotacaoCol.Date != dataVotacao.Date)
                    continue;

                var idUsuarioCol = colunas[1];
                var idRestauranteCol = colunas[2];

                var usuario = await _usuarioRepositorio.ObterUsuario(int.Parse(idUsuarioCol));
                var restaurante = await _restauranteRepositorio.ObterRestaurante(int.Parse(idRestauranteCol));

                var voto = new Voto(usuario, restaurante);

                votos.Add(voto);
            }

            return votos;
        }
    }
}
