﻿using Microsoft.Extensions.Options;
using OndeComer.ClassLibrary.models;
using OndeComer.ClassLibrary.repositorios;
using OndeComer.ClassLibrary.settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace OndeComer.Infra.Repositorios
{
    public class RestauranteRepositorioCsv : IRestauranteRepositorio
    {
        private readonly string CaminhoRestauranteCsv;

        public RestauranteRepositorioCsv(IOptionsMonitor<ArquivosSettings> arquivosSettings)
        {
            CaminhoRestauranteCsv = arquivosSettings.CurrentValue.RestaurantesCsvPath;
        }

        public void AtualizarRestaurante(Restaurante restaurante)
        {
            throw new NotImplementedException();
        }

        public async void InserirRestaurante(Restaurante restaurante)
        {
            var existeRestaurante = (await ObterRestaurante(restaurante.IdRestaurante)) != null;

            if (existeRestaurante)
                throw new Exception("O id de restaurante informado já existe");

            var rowsRestaurante = await File.ReadAllLinesAsync(CaminhoRestauranteCsv);

            var novosRestaurantes = rowsRestaurante.ToList();
            novosRestaurantes.Add($"{restaurante.IdRestaurante},{restaurante.Nome}");

            await File.WriteAllLinesAsync(CaminhoRestauranteCsv, novosRestaurantes);
        }

        public Task<List<Restaurante>> ListarRestaurante()
        {
            throw new NotImplementedException();
        }

        public Task<Restaurante> ObterInteralRestaurante(long idRestaurante)
        {
            throw new NotImplementedException();
        }

        public async Task<Restaurante> ObterRestaurante(int idRestaurante)
        {
            var linhas = await File.ReadAllLinesAsync(CaminhoRestauranteCsv);

            foreach (var linha in linhas)
            {
                var restaurante = ConverterStringParaRestaurante(linha);

                if (restaurante.IdRestaurante == idRestaurante)
                    return restaurante;
            }

            return null;
        }

        public Task<Restaurante> ObterRestaurante(long idRestaurante)
        {
            throw new NotImplementedException();
        }

        private Restaurante ConverterStringParaRestaurante(string linha)
        {
            var colunas = linha.Split(',');
            var colunaId = int.Parse(colunas[0]);
            var colunaNome = colunas[1];

            return new Restaurante(colunaId, colunaNome);
        }
    }
}
