﻿using Microsoft.Extensions.Options;
using OndeComer.ClassLibrary.models;
using OndeComer.ClassLibrary.repositorios;
using OndeComer.ClassLibrary.settings;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace OndeComer.Infra.Repositorios
{
    public class UsuarioRepositorioCsv : IUsuarioRepositorio
    {
        private readonly string CaminhoUsuarioCsv;

        public UsuarioRepositorioCsv(IOptionsMonitor<ArquivosSettings> arquivosSettings)
        {
            CaminhoUsuarioCsv = arquivosSettings.CurrentValue.UsuariosCsvPath;
        }

        public async Task InserirUsuario(Usuario usuario)
        {
            var existeUsuario = (await ObterUsuario(usuario.IdUsuario)) != null;

            if (existeUsuario)
                throw new Exception("O id de usuário informado já existe");

            var rowsUsuarios = await File.ReadAllLinesAsync(CaminhoUsuarioCsv);

            var novosUsuarios = rowsUsuarios.ToList();
            novosUsuarios.Add($"{usuario.IdUsuario},{usuario.Nome}");

            await File.WriteAllLinesAsync(CaminhoUsuarioCsv, novosUsuarios);
        }

        public async Task<Usuario> ObterUsuario(int idUsuario)
        {
            var linhas = await File.ReadAllLinesAsync(CaminhoUsuarioCsv);

            foreach (var linha in linhas)
            {
                var usuario = ConverterStringParaUsuario(linha);

                if (usuario.IdUsuario == idUsuario)
                    return usuario;
            }

            return null;
        }

        private Usuario ConverterStringParaUsuario(string linha)
        {
            var colunas = linha.Split(',');
            var colunaId = int.Parse(colunas[0]);
            var colunaNome = colunas[1];

            return new Usuario(colunaId, colunaNome);
        }
    }
}
