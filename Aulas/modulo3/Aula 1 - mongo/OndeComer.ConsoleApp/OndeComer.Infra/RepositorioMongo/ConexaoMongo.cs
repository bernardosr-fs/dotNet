﻿using Microsoft.Extensions.Configuration;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using OndeComer.ClassLibrary.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OndeComer.Infra.RepositorioMongo
{
    public class ConexaoMongo
    {
        public IMongoDatabase DB { get; set; }

        public ConexaoMongo(IConfiguration configuration)
        {
            try
            {
                var settings = MongoClientSettings.FromUrl(new MongoUrl(configuration["ConnectionMongo"]));
                var client = new MongoClient(settings);
                DB = client.GetDatabase(configuration["NomeBancoMongo"]);
                MappClass();
            }
            catch (Exception e)
            {

                throw;
            }
        }

        private void MappClass()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(Votacao)))
            {
                BsonClassMap.RegisterClassMap<Votacao>(i =>
                {
                    i.AutoMap();
                    i.SetIgnoreExtraElements(true);
                    i.UnmapMember(m => m.Id);
                });
            }
            if (!BsonClassMap.IsClassMapRegistered(typeof(Voto)))
            {
                BsonClassMap.RegisterClassMap<Voto>(i =>
                {
                    i.AutoMap();
                    i.SetIgnoreExtraElements(true);
                });
            };
            if (!BsonClassMap.IsClassMapRegistered(typeof(Usuario)))
            {
                BsonClassMap.RegisterClassMap<Usuario>(i =>
                {
                    i.AutoMap();
                    i.SetIgnoreExtraElements(true);
                });
            }
            if (!BsonClassMap.IsClassMapRegistered(typeof(Restaurante)))
            {
                BsonClassMap.RegisterClassMap<Restaurante>(i =>
                {
                    i.AutoMap();
                    i.SetIgnoreExtraElements(true);
                });
            }


        }

    }
}
