﻿using Hangfire.Mongo;
using MongoDB.Bson;
using MongoDB.Driver;
using OndeComer.ClassLibrary.models;
using OndeComer.ClassLibrary.repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OndeComer.Infra.RepositorioMongo
{
    public class VotacaoRepositorio : IVotacaoRepositorioMongo
    {
        IMongoCollection<Votacao> _votacao;

        public VotacaoRepositorio(ConexaoMongo mongoDB)
        {
            _votacao = mongoDB.DB.GetCollection<Votacao>("Votacao");
        }

        public Task Inserir(Votacao votacao)
        {
                return _votacao.InsertOneAsync(votacao);
        }

        public async Task<List<Votacao>> Listar()
        {
            try
            {
                var filtro = Builders<Votacao>.Filter.Empty;
                return await _votacao.AsQueryable().ToListAsync();
            }
            catch (Exception ex)
            {

                throw;
            }
        
        }
    }
}
