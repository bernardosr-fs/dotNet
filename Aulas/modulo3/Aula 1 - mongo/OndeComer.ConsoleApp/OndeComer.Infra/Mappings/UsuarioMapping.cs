﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OndeComer.ClassLibrary.models;

namespace OndeComer.Infra.Mappings
{
    public class UsuarioMapping : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.Property(c => c.IdUsuario)
                .HasColumnName("Id");

            builder.Property(c => c.Nome)
                .HasColumnName("Nome");
        }
    }
}
