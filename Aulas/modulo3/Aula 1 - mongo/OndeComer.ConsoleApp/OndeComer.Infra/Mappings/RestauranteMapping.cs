﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OndeComer.ClassLibrary.models;

namespace OndeComer.Infra.Mappings
{
    public class RestauranteMapping : IEntityTypeConfiguration<Restaurante>
    {
        public void Configure(EntityTypeBuilder<Restaurante> builder)
        {
            builder.Property(c => c.IdRestaurante)
                .HasColumnName("Id");

            builder.HasKey(c => c.IdRestaurante);

            builder.Property(c => c.Nome)
                .HasColumnName("Nome");
        }
    }
}
