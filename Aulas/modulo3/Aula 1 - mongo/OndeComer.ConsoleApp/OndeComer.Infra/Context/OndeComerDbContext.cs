﻿using Microsoft.EntityFrameworkCore;
using OndeComer.ClassLibrary.models;
using OndeComer.Infra.Mappings;

namespace OndeComer.Infra.Context
{
    public class OndeComerDbContext : DbContext
    {
        public OndeComerDbContext(DbContextOptions<OndeComerDbContext> dbContextOptions) : base (dbContextOptions)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new RestauranteMapping());

            modelBuilder.Entity<Restaurante>()
                .Property(f => f.IdRestaurante)
                .ValueGeneratedOnAdd();
            //modelBuilder.ApplyConfiguration(new UsuarioMapping());
            //modelBuilder.ApplyConfiguration(new VotacaoMapping());
        }

        public DbSet<Restaurante> Restaurantes { get; set; }
        //public DbSet<Usuario> Usuarios { get; set; }
        //public DbSet<Votacao> Votacoes { get; set; }
    }
}
